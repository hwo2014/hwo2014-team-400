package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    private double throttle = 1.0;
    private double lastAngle = 0.0;
    private double lastPieceDistance = 0;
    
    private String myColor = null;
	Map<Integer, Boolean> switchRight = new HashMap<Integer, Boolean>();
	Set<Integer> anglePieces = new HashSet<Integer>();
	Set<Integer> onTurn = new HashSet<Integer>();
	
    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//            System.out.println("Received: " + line);
//            System.out.println("Received: " + line);
            if (msgFromServer.msgType.equals("carPositions")) {
            	List<Map<String, Object>> carList = (List<Map<String, Object>>)msgFromServer.data;
            	for(Map<String, Object> car : carList) {
//            		System.out.println("Received car: " + car);
            		if(((Map<String, String>)car.get("id")).get("color").equals(myColor)) {
            			double angle = Math.abs((Double)car.get("angle"));
            			double angleDelta = angle - lastAngle;
            			Object pieceIndex = get(get(car, "piecePosition"), "pieceIndex");
            			int inPieceDistance = ((Double)get(get(car, "piecePosition"), "inPieceDistance")).intValue();
            			
            			Object lap = get(get(car, "piecePosition"), "lap");
            			lastAngle = angle;
            			double oldThrottle = throttle;
            			//if(angle > 10) { // remove this in favor of angleDelta>1 check
            			//	throttle = 0.0;
            			if(angleDelta > 3) {
            				throttle = 0.0;
            			} else if(angle > 15 && angleDelta > 0.0001) {
            				throttle = 0.1;
            			} else {
            				throttle = 1.0;
            			}
//            			if(angle == 0) {
//            				throttle = 1.0; //Math.min(1.0, throttle + 0.05);
//            			} else if (angle < 3.0) {
//            				throttle = Math.min(1.0, throttle + .05);
//            			} else if (angle < 1.0) {
//            				throttle = Math.min(1.0, throttle * .5);
//            			} else if (angle < 5) {
//        					throttle = Math.max(0.01, throttle * 0.75);
//        				} else if (angle < 10) {
//        					throttle = Math.max(0.01, throttle * 0.5);
//            			} else {
//            				throttle = Math.max(0.05, 0.05);
//            			}
            			System.out.printf("Angle=%s, AngleDelta=%s, pieceIndex=%s, pieceDistance=%s, lap=%s%n", angle, angleDelta, pieceIndex, inPieceDistance, lap);

            			Integer curPieceIndex = Double.valueOf(""+get(get(car, "piecePosition"), "pieceIndex")).intValue();
            			
            			if(anglePieces.contains(curPieceIndex+1) && inPieceDistance - lastPieceDistance > 7) {
            				throttle = Math.min(throttle, 0.0);
            			}
            			lastPieceDistance = inPieceDistance;
            			
            			if(throttle != oldThrottle) {
            				send(new Throttle(throttle));
            			} else {
	            			Boolean turnRight = null;
	            			for(int i = 0; i < 100; i++) {
	            				turnRight = switchRight.get(curPieceIndex+i);
	            				if(turnRight != null) {
	            					break;
	            				}
	            			}
	            			if(!onTurn.contains(curPieceIndex) && turnRight != null) {
	            				send(new Switch(turnRight));
	            			} else {
	            				send(new Throttle(throttle));
	            			}
	            			onTurn.clear();
	            			onTurn.add(curPieceIndex);
            			}
            			// System.out.println(" Throttle=" + throttle +"\tangle=" + angle);
            		}
            	}
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	List<Map<String, Object>> pieces = get(get(get(msgFromServer.data, "race"), "track"), "pieces");
            	double cumulativeArcLength = 0;
            	for(int i = pieces.size() - 1; i >= 0; i--) {
            		System.out.println("Checking piece: " + i);
            		Map<String, Object> piece = pieces.get(i);
            		// walk backward in the pieces figuring out the best side
            		// to be on...when we get to a switch piece, add an entry
            		if(piece.containsKey("angle")) {
            			anglePieces.add(i);
            			Double angle = (Double)piece.get("angle");
            			Double radius = (Double)piece.get("radius");
            			double arcLength = (angle / 360.0) * (radius*2) * 3.1415;
            			cumulativeArcLength += arcLength;
            			System.out.printf(" Angle=%s, radius=%s, arcL=%s, cumArcL=%s%n", angle, radius, arcLength, cumulativeArcLength);
            		}
            		if("true".equalsIgnoreCase(""+piece.get("switch"))) {
            			System.out.println(" Switch piece...turning right? " + (cumulativeArcLength >= 0));
            			switchRight.put(i, cumulativeArcLength >= 0);
            			cumulativeArcLength = 0;
            		}
            	}
            	
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("yourCar")) {
            	myColor = get(msgFromServer.data, "color");
            } else {
            	System.out.println("Received: " + line);
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
    	System.out.println("Sending: " + msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }
    
    private <T> T get(Object map, String key) {
    	return (T)((Map)map).get(key);
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class JoinRace extends SendMsg {
	public final Map<String, String> botId;
	public final int carCount;

    JoinRace(final String name, final String key, final int carCount) {
    	botId = new HashMap<String, String>();
    	botId.put("name", name);
    	botId.put("key", key);
    	this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Switch extends SendMsg {
    private boolean switchRight;

    public Switch(boolean switchRight) {
        this.switchRight = switchRight;
    }

    @Override
    protected Object msgData() {
    	return switchRight ? "Right" : "Left";
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}